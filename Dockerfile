FROM openjdk:8
EXPOSE 8090
ENV SPRING_PROFILE=mysql
COPY target/assignment-*.jar /usr/local/app.jar
RUN chmod +x /usr/local/app.jar
ENTRYPOINT sleep 20; java -jar -Dspring.profiles.active=${SPRING_PROFILE} /usr/local/app.jar